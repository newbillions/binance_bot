import sys
from binance.client import Client
import sched, time
import random

MAX_RETRY = 3

def core_business_logic(s, b2, SLEEP_TIME, MARKET, IS_BUY, current_token_amount, TOTAL_TOKEN_AMOUNT, BUY_SELL_WALL_PRICE,TOKEN_RAND_UPPER_LIMIT,TOKEN_RAND_LOWER_LIMIT,IS_MARKET_ORDER,NUMBER_OF_ORDERS_PER_EXEC,DEBUG):
    if DEBUG: print("[DEBUG]entering core_business_logic\n[DEBUG]SLEEP_TIME:{}, MARKET:{}, IS_BUY:{}, current_token_amount:{}, TOTAL_TOKEN_AMOUNT:{}, BUY_SELL_WALL_PRICE:{},TOKEN_RAND_UPPER_LIMIT:{} ,TOKEN_RAND_LOWER_LIMIT:{}".format(SLEEP_TIME, MARKET, IS_BUY, current_token_amount, TOTAL_TOKEN_AMOUNT, BUY_SELL_WALL_PRICE,TOKEN_RAND_UPPER_LIMIT,TOKEN_RAND_LOWER_LIMIT))
    print("[Progress] {}/{} Token for {}\n".format(current_token_amount,TOTAL_TOKEN_AMOUNT,MARKET))

    # Check current_token_amount < TOTAL_TOKEN_AMOUNT
    if current_token_amount >= TOTAL_TOKEN_AMOUNT:
        print("[INFO] current_token_amount:{} >= TOTAL_TOKEN_AMOUNT:{} exit the program".format(current_token_amount,TOTAL_TOKEN_AMOUNT))
        return

    # Try to get market_accept_price
    retry_count = 0
    while (retry_count < MAX_RETRY):
        retry_count += 1
        try:
            if IS_BUY:
                order_book = b2.get_order_book(symbol=MARKET)["bids"]
                order_book.sort(key=lambda x: x[0],reverse=True)
            else:
                order_book = b2.get_order_book(symbol=MARKET)["asks"]
                order_book.sort(key=lambda x: x[0])
            break
        except:
            if DEBUG: print("[ERROR]retry get_order_book "+str(retry_count))
            continue
    if retry_count >= MAX_RETRY:
        s.enter(SLEEP_TIME, 1, core_business_logic, (s, b2, SLEEP_TIME, MARKET, IS_BUY, current_token_amount, TOTAL_TOKEN_AMOUNT, BUY_SELL_WALL_PRICE,TOKEN_RAND_UPPER_LIMIT,TOKEN_RAND_LOWER_LIMIT,IS_MARKET_ORDER,NUMBER_OF_ORDERS_PER_EXEC,DEBUG))
        print("[ERROR] fail to get_order_book after {} trails. sleep for {} and wait for next time".format(MAX_RETRY,SLEEP_TIME))
        return

    market_accept_price = float(order_book[0][0])
    print("\t[INFO]market_accept_price:%.10f\n" % (market_accept_price))

    # Test the BUY_SELL_WALL_PRICE condition
    if IS_BUY:
        if market_accept_price > BUY_SELL_WALL_PRICE:
            print("[INFO] BUY {} is GREATER than {}.sleep for {} and wait for next time".format(market_accept_price,BUY_SELL_WALL_PRICE,SLEEP_TIME))
            s.enter(SLEEP_TIME, 1, core_business_logic, (s, b2, SLEEP_TIME, MARKET, IS_BUY, current_token_amount, TOTAL_TOKEN_AMOUNT, BUY_SELL_WALL_PRICE,TOKEN_RAND_UPPER_LIMIT,TOKEN_RAND_LOWER_LIMIT,IS_MARKET_ORDER,NUMBER_OF_ORDERS_PER_EXEC,DEBUG))
            return
    else:
        if market_accept_price < BUY_SELL_WALL_PRICE:
            print("[INFO] SELL {} is SMALLER than {}.sleep for {} and wait for next time".format(market_accept_price,BUY_SELL_WALL_PRICE,SLEEP_TIME))
            s.enter(SLEEP_TIME, 1, core_business_logic, (s, b2, SLEEP_TIME, MARKET, IS_BUY, current_token_amount, TOTAL_TOKEN_AMOUNT, BUY_SELL_WALL_PRICE,TOKEN_RAND_UPPER_LIMIT,TOKEN_RAND_LOWER_LIMIT,IS_MARKET_ORDER,NUMBER_OF_ORDERS_PER_EXEC,DEBUG))
            return

    retry_count = 0
    while (retry_count < MAX_RETRY):
        retry_count += 1
        try:
            m_token_amount = random.randint(TOKEN_RAND_LOWER_LIMIT,TOKEN_RAND_UPPER_LIMIT)
            if IS_BUY: side = "BUY"
            else: side = "SELL"
            for x in range(NUMBER_OF_ORDERS_PER_EXEC):
                # Check current_token_amount < TOTAL_TOKEN_AMOUNT
                if current_token_amount >= TOTAL_TOKEN_AMOUNT:
                    print("[INFO] current_token_amount:{} >= TOTAL_TOKEN_AMOUNT:{} exit the program".format(current_token_amount,TOTAL_TOKEN_AMOUNT))
                    return
                if IS_MARKET_ORDER: order = b2.create_order(disable_validation=True,symbol=MARKET,side=side,type="MARKET",quantity=m_token_amount)
                else: order = b2.create_order(disable_validation=True,symbol=MARKET,side=side,type="LIMIT",quantity=m_token_amount,timeInForce="GTC",price=market_accept_price)
                current_token_amount += m_token_amount
                print("\t{} Order:{}\n".format(x+1,order))
            break
        except Exception as e:
            if DEBUG: print("[DEBUG] retry create_order {}count. {}".format(str(retry_count),e))
            if retry_count == MAX_RETRY: print("[ERROR]fail to create order after {} times. give up. {}".format(retry_count,e))
            continue

    s.enter(SLEEP_TIME, 1, core_business_logic, (s, b2, SLEEP_TIME, MARKET, IS_BUY, current_token_amount, TOTAL_TOKEN_AMOUNT, BUY_SELL_WALL_PRICE,TOKEN_RAND_UPPER_LIMIT,TOKEN_RAND_LOWER_LIMIT,IS_MARKET_ORDER,NUMBER_OF_ORDERS_PER_EXEC,DEBUG))

def binance_order(API_KEY,API_SECRET,SLEEP_TIME,MARKET,IS_BUY,TOTAL_TOKEN_AMOUNT,BUY_SELL_WALL_PRICE,TOKEN_RAND_UPPER_LIMIT,TOKEN_RAND_LOWER_LIMIT,IS_MARKET_ORDER,NUMBER_OF_ORDERS_PER_EXEC,DEBUG):
    b2 = Client(API_KEY,API_SECRET)

    s = sched.scheduler(time.time, time.sleep)
    s.enter(1, 1, core_business_logic, (s,b2,SLEEP_TIME,MARKET,IS_BUY, 0, TOTAL_TOKEN_AMOUNT,BUY_SELL_WALL_PRICE,TOKEN_RAND_UPPER_LIMIT,TOKEN_RAND_LOWER_LIMIT,IS_MARKET_ORDER,NUMBER_OF_ORDERS_PER_EXEC,DEBUG))
    s.run()
