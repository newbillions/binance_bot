import sys
from binance.client import Client
import sched, time
import random

# User should pass the encry_pass through the command line
encry_pass = sys.argv[1]
b2 = Client(encry_pass,"txts/binance")

s = sched.scheduler(time.time, time.sleep)
SLEEP_TIME = 5 # TODO 

PRICE = (1,2) # Average of 1st and 2nd price
BUY_TOKEN = "RDNETH"

MAX_RETRY = 3

def do_something(sc):
    print("Doing stuff...")
    EOS_AMOUNT = random.randint(100,700)
    retry_count = 0
    while (retry_count < MAX_RETRY):
        retry_count += 1
        print("retry get_order_book "+str(retry_count))
        try:
            order_book = b2.get_order_book(symbol=BUY_TOKEN)
            buy_order_book = order_book['bids']
            sell_order_book = order_book['asks']
            break
        except:
            continue
    if retry_count >= MAX_RETRY:
        s.enter(SLEEP_TIME, 1, do_something, (sc,))
        return
    #price = (float(buy_order_book[PRICE[0]][0]) + float(sell_order_book[PRICE[0]][0]))/2  ((float(sell_order_book[PRICE[0]][0])-float(buy_order_book[PRICE[0]][0])))/3
    price = (float(buy_order_book[PRICE[0]][0]) + float(buy_order_book[PRICE[1]][0])) / 2
    price = "{0:.6f}".format(price)
    print("BUY highest_price:{}".format(buy_order_book[PRICE[0]][0]))
    print("SELL lowest_price:{}".format(sell_order_book[PRICE[0]][0]))
    print("our_price:{}".format(str(price)))
    retry_count = 0
    while (retry_count < MAX_RETRY):
        retry_count += 1
        try:
	    i = random.randint(1,100)
            print(i)
	    if(i > 40):    #60% sell
                buy = b2.create_order(symbol=BUY_TOKEN,side="BUY",type="LIMIT",timeInForce="GTC",quantity=EOS_AMOUNT,price=price)
                sell = b2.create_order(symbol=BUY_TOKEN,side="SELL",type="LIMIT",timeInForce="GTC",quantity=EOS_AMOUNT,price=price)
                print(buy)
                print(sell)
            else:          #40% buy
	        print("prepare to buy")
		sell = b2.create_order(symbol=BUY_TOKEN,side="SELL",type="LIMIT",timeInForce="GTC",quantity=EOS_AMOUNT,price=price)
	        buy = b2.create_order(symbol=BUY_TOKEN,side="BUY",type="LIMIT",timeInForce="GTC",quantity=EOS_AMOUNT,price=price)	
   	        print(sell)
                print(buy)
            break
        except Exception as e:
            print("retry create_order "+str(retry_count))
            print(e)
            continue
    if retry_count >= MAX_RETRY:
        print("fail to create order after {} times. give up".format(retry_count))

    s.enter(SLEEP_TIME, 1, do_something, (sc,))

s.enter(1, 1, do_something, (s,))
s.run()
