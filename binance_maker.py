import sys
from binance.client import Client
import numpy as np, numpy.random

MAX_RETRY = 3
FLOAT_PRECISION = 8

def create_order_helper(b2,MARKET,side,quantity,price):
    for x in range(MAX_RETRY):
        try:
            order = b2.create_order(disable_validation=True,symbol=MARKET,side=side,type="LIMIT",quantity=quantity,timeInForce="GTC",price=price)
            print("\torder placed")
            break
        except Exception as e:
            print("\t[ERROR] Fail to create order for {} {} {} {}. {} Retry".format(MARKET,side,quantity,price,e))

def core_business_logic(b2,MARKET,MID,BID_MARGIN,BID_LAYERS,BID_LAYER_STEP,INIT_BUY,ASK_MARGIN,ASK_LAYERS,ASK_LAYER_STEP,INIT_SELL,PRICE_LAYER_STEP,Should_trade):
    # Build ask_price_arr
    ask_price_start = MID + ASK_MARGIN
    ask_price_arr = [round(ask_price_start,FLOAT_PRECISION)]
    for x in range(ASK_LAYERS):
        t_price = ask_price_start + (x+1) * PRICE_LAYER_STEP
        t_price = round(t_price,FLOAT_PRECISION)
        ask_price_arr.append(t_price)

    # build ask_quantity_arr
    ask_start_quantity = INIT_SELL
    ask_quantity_arr = [ask_start_quantity]
    for x in range(ASK_LAYERS):
        ask_start_quantity += ASK_LAYER_STEP
        ask_quantity_arr.append(ask_start_quantity)

    # Build bid_price_arr
    bid_price_start = MID - BID_MARGIN
    bid_price_arr = [round(bid_price_start,FLOAT_PRECISION)]
    for x in range(BID_LAYERS):
        t_price = bid_price_start - (x+1) * PRICE_LAYER_STEP
        t_price = round(t_price,FLOAT_PRECISION)
        bid_price_arr.append(t_price)

    # build bid_quantity_arr
    bid_start_quantity = INIT_BUY
    bid_quantity_arr = [bid_start_quantity]
    for x in range(BID_LAYERS):
        bid_start_quantity += BID_LAYER_STEP
        bid_quantity_arr.append(bid_start_quantity)


    for x in range(ASK_LAYERS,-1,-1):
        print("ASK {} {}".format(ask_price_arr[x],ask_quantity_arr[x]))
        if Should_trade: create_order_helper(b2,MARKET,"SELL",ask_quantity_arr[x],ask_price_arr[x])

    print("---------------------------------------")
    for x in range(0,BID_LAYERS+1):
        print("BID {} {}".format(bid_price_arr[x],bid_quantity_arr[x]))
        if Should_trade: create_order_helper(b2,MARKET,"BUY",bid_quantity_arr[x],bid_price_arr[x])

def binance_maker(API_KEY,API_SECRET,MARKET,MID,BID_MARGIN,BID_LAYERS,BID_LAYER_STEP,INIT_BUY,ASK_MARGIN,ASK_LAYERS,ASK_LAYER_STEP,INIT_SELL,PRICE_LAYER_STEP,Should_trade):
    b2 = Client(API_KEY,API_SECRET)

    core_business_logic(b2,MARKET,MID,BID_MARGIN,BID_LAYERS,BID_LAYER_STEP,INIT_BUY,ASK_MARGIN,ASK_LAYERS,ASK_LAYER_STEP,INIT_SELL,PRICE_LAYER_STEP,Should_trade)
